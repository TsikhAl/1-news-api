var gulp = require('gulp');
var concat = require('gulp-concat');

gulp.task('concatJS', function() {
  return gulp.src('js/*.js')
    .pipe(concat('main_concat.js'))
    .pipe(gulp.dest('js/main_concat'));
});

gulp.task('default', ['concatJS']);
