(function () {

let createNewsItem = (data) => {
    if ( !data ) return '';

    let container = document.getElementById('news-content');

    if (container.children.length) {
        container.innerHTML = '';
    }

    for (let i = 0; i < data.articles.length; i++) {

        let author = data.articles[i].author || '',
            title = data.articles[i].title || '',
            description = data.articles[i].description || '',
            publishedAt = data.articles[i].publishedAt || '',
            url = data.articles[i].url || '',
            newsItemContent = `<div class='news-content-item clearfix'><h1>${title}</h1><h2>The author:  ${author}</h2>` +
                            `<p>${description}</p><img src='${data.articles[i].urlToImage}' alt='Sorry, no photo  =('>` +
                            `<span>Published at: ${publishedAt}</span><span>This resource is taken from: <a href='https://newsapi.org/'>News API</a>. If you want more info about this article: <a href='${url}'>CHECK IT</a></span></div>`;

        container.insertAdjacentHTML("beforeEnd", newsItemContent);
    }
}

let getNews = (src) => {
    let url = `https://newsapi.org/v2/top-headlines?` +
              `sources=${src}&` +
              `apiKey=d6ef3e11c191461c886ae8204f3af1e9`,
        req = new Request(url);

    fetch(req)
        .then( (response) => {
            let resp = (response.json());
                resp.then( (data) => {
                    createNewsItem(data);
                })
    })
}

let iconsContainer = document.getElementById('news-icons');
iconsContainer.addEventListener('click', (event) => {

    if (event.target.tagName === 'SPAN' || event.target.tagName === 'IMG' || event.target.tagName === 'LI') {

        let src;
        // for IE 10
        if (window.navigator.appName === 'Microsoft Internet Explorer') {

            src = (event.target.tagName === 'IMG') ? event.target.parentElement.parentElement.attributes[1].textContent :
                (event.target.tagName === 'SPAN') ? event.target.parentElement.attributes[1].textContent :  event.target.attributes[1].textContent;
        } else {

            src = (event.target.tagName === 'IMG') ? event.target.parentElement.parentElement.dataset.src :
                (event.target.tagName === 'SPAN') ? event.target.parentElement.dataset.src :  event.target.dataset.src;

        }

        getNews(src);
    }
});

})();
